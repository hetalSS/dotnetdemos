﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BansiDemo.Models;

namespace BansiDemo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult AddStudent()
        {
            var c = new Class();

            Student s = new Student();
            s.Name = "Bansi";
            c.Students.Add(s);
            int a = c.SaveChanges();

            if (a > 0)
            {
                ViewBag.Msg = "Data Added";
            }
            else {
                ViewBag.Msg = "Try Again";
            }


            return View("Index");


        }
    }
}
